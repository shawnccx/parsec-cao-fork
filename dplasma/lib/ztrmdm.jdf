extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 * $COPYRIGHT
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

extern int CORE_ztrmdm(int uplo, int N, PLASMA_Complex64_t *A, int LDA);

%}

desc_A  [type = "tiled_matrix_desc_t"]
data_A  [type = "dague_ddesc_t *"]

ztrmdm(k)
  /* Execution space */
  k = 0..(desc_A.mt-2)

  : data_A(k,k)

  /* A == data_A(k,k) */

  RW  A <- data_A(k,k)
        -> data_A(k,k)

BODY

  int tempkn = ((k)==((desc_A.nt)-1)) ? ((desc_A.n)-(k*(desc_A.nb))) : (desc_A.nb);
  int ldak = desc_A.mb;

  printlog("thread %d CORE_ztrmdm(%d)\n"
           "\t(PlasmaLower, tempkn, A(%d,%d)[%p], ldak)\n",
           context->th_id, k, k, k, A);
#if !defined(DAGUE_DRY_RUN)
    CORE_ztrmdm(PlasmaLower, tempkn, A /* desc_A(k,k) */, ldak );
#endif /* !defined(DAGUE_DRY_RUN) */


END
