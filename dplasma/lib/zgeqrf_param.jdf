extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 * $COPYRIGHT
 *
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

//#define PRIO_YVES1

#if defined(PRIO_YVES1)
#define GETPRIO_PANEL( __m, __n )      descA.mt * descA.nt - ((descA.nt - (__n) - 1) * descA.mt + (__m) + 1)
#define GETPRIO_UPDTE( __m, __n, __k ) descA.mt * descA.nt - ((descA.nt - (__n) - 1) * descA.mt + (__m) + 1)
#elif defined(PRIO_YVES2)
#define GETPRIO_PANEL( __m, __n )      descA.mt * descA.nt - ((__m) * descA.nt + descA.nt - (__n))
#define GETPRIO_UPDTE( __m, __n, __k ) descA.mt * descA.nt - ((__m) * descA.nt + descA.nt - (__n))
#elif defined(PRIO_MATHIEU1)
#define GETPRIO_PANEL( __m, __n )      (descA.mt + (__n) - (__m) - 1) * descA.nt + (__n)
#define GETPRIO_UPDTE( __m, __n, __k ) (descA.mt + (__n) - (__m) - 1) * descA.nt + (__n)
#elif defined(PRIO_MATHIEU2)
#define GETPRIO_PANEL( __m, __n )      ((dplasma_imax(descA.mt, descA.nt) - dplasma_imax( (__n) - (__m), (__m) - (__n) ) -1 ) * 12 + (__n))
#define GETPRIO_UPDTE( __m, __n, __k ) ((dplasma_imax(descA.mt, descA.nt) - dplasma_imax( (__n) - (__m), (__m) - (__n) ) -1 ) * 12 + (__n))
#elif defined(PRIO_MATYVES)
#define FORMULE( __x ) ( ( -1. + dplasma_dsqrt( 1. + 4.* (__x) * (__x)) ) * 0.5 )
#define GETPRIO_PANEL( __m, __k )      (int)( 22. * (__k) + 6. * ( FORMULE( descA.mt ) - FORMULE( (__m) - (__k) + 1. ) ) )
#define GETPRIO_UPDTE( __m, __n, __k ) (int)( (__m) < (__n) ? GETPRIO_PANEL( (__n), (__n) ) - 22. * ( (__m) - (__k) ) - 6. * ( (__n) - (__m) ) \
                                              :               GETPRIO_PANEL( (__m), (__n) ) - 22. * ( (__n) - (__k) ) )
#else
  /*#warning running without priority*/
#define GETPRIO_PANEL( __m, __n )      0
#define GETPRIO_UPDTE( __m, __n, __k ) 0
#endif

%}

dataA  [type = "dague_ddesc_t *"]
descA  [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
dataTS [type = "dague_ddesc_t *" aligned=dataA]
descTS [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataTS)"]
dataTT [type = "dague_ddesc_t *" aligned=dataA]
descTT [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataTT)"]
qrtree [type = "dplasma_qrtree_t"]
ib     [type = "int"]
p_work [type = "dague_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)*ib*(descTS.nb))"]
p_tau  [type = "dague_memory_pool_t *" size = "(sizeof(PLASMA_Complex64_t)   *(descTS.nb))"]

minMN  [type = "int" hidden=on default="( (descA.mt < descA.nt) ? descA.mt : descA.nt )" ]

/**
 * zgeqrt()
 *
 * There are dplasma_qr_getnbgeqrf( pivfct, k, descA.mt ) geqrt applyed at step
 * k on the rows indexed by m.
 * nextm is the first row that will be killed by the row m at step k.
 * nextm = descA.mt if the row m is never used as a killer.
 *
 */
zgeqrt(k, i)
  /* Execution space */
  k = 0 .. minMN-1
  i = 0 .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  m      = inline_c %{ return qrtree.getm(       &qrtree, k, i); %}
  nextm  = inline_c %{ return qrtree.nextpiv(    &qrtree, k, m, descA.mt); %}

  SIMCOST 4

  /* Locality */
  : dataA(m, k)

  RW    A <- ( k == 0 ) ? dataA(m, k)
          <- ( k >  0 ) ? A2 zttmqr(k-1, m, k )

          -> A zgeqrt_typechange(k, i)

          -> ( k == descA.mt-1 ) ? dataA(m, k)                                 [type = UPPER_TILE]
          -> ( (k < descA.mt-1) & (nextm != descA.mt) ) ?  A1 zttqrt(k, nextm) [type = UPPER_TILE]
          -> ( (k < descA.mt-1) & (nextm == descA.mt) ) ?  A2 zttqrt(k, m)     [type = UPPER_TILE]

  RW    T <- dataTS(m, k)                                                      [type = LITTLE_T]
          -> dataTS(m, k)                                                      [type = LITTLE_T]
          -> (k < descA.nt-1) ? T zunmqr(k, i, (k+1)..(descA.nt-1))            [type = LITTLE_T]

  ; inline_c %{ return GETPRIO_PANEL(m, k); %}

BODY
{
    int tempmm = (m==(descA.mt-1)) ? (descA.m - m * descA.mb) : descA.mb;
    int tempkn = (k==(descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldam   = BLKLDD( descA, m );

    printlog("thread %d VP %d CORE_zgeqrt(%d, %d)\n"
             "\t(tempmm, tempkn, ib, dataA(%d,%d)[%p], ldam, dataTS(%d,%d)[%p], TS.mb, p_elem_A, p_elem_B)\n",
             context->th_id, context->virtual_process->vp_id, k, m, m, k, A, m, k, T);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_tau  );
    void *p_elem_B = dague_private_memory_pop( p_work );

    CORE_zgeqrt(tempmm, tempkn, ib,
                A /* dataA(m,k)  */, ldam,
                T /* dataTS(m,k) */, descTS.mb,
                p_elem_A, p_elem_B );

    dague_private_memory_push( p_tau, p_elem_A );
    dague_private_memory_push( p_work, p_elem_B );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**
 * zgeqrt_typechange()
 *
 * Task to distinguish upper/lower part of the tile
 */
zgeqrt_typechange(k, i) [profile = off]
  /* Execution space */
  k = 0 .. minMN-1
  i = 0 .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  m =      inline_c %{ return qrtree.getm(       &qrtree, k, i); %}

  : dataA(m, k)

  RW A <- A zgeqrt(k, i)
       -> ( k < (descA.nt-1) ) ? A zunmqr(k, i, (k+1)..(descA.nt-1)) [type = LOWER_TILE]
       -> dataA(m, k)                                                [type = LOWER_TILE]
BODY
{
    /* Nothing */
}
END

/**
 * zunmqr()
 *
 * (see zgeqrt() for details on definition space)
 */
zunmqr(k, i, n)
  /* Execution space */
  k = 0   .. minMN-1
  i = 0   .. inline_c %{ return qrtree.getnbgeqrf( &qrtree, k ) - 1; %}
  n = k+1 .. descA.nt-1
  m     = inline_c %{ return qrtree.getm(    &qrtree, k, i); %}
  nextm = inline_c %{ return qrtree.nextpiv( &qrtree, k, m, descA.mt); %}

  SIMCOST 6

  /* Locality */
  : dataA(m, n)

  READ  A <- A zgeqrt_typechange(k, i)                              [type = LOWER_TILE]
  READ  T <- T zgeqrt(k, i)                                         [type = LITTLE_T]

  RW    C <- ( 0 == k ) ? dataA(m, n)
          <- ( k >  0 ) ? A2 zttmqr(k-1, m, n)
          -> ( k == (descA.mt-1)) ? dataA(m, n)
          -> ((k <  (descA.mt-1)) & (nextm != descA.mt) ) ? A1 zttmqr(k, nextm, n)
          -> ((k <  (descA.mt-1)) & (nextm == descA.mt) ) ? A2 zttmqr(k, m,     n)

    ; inline_c %{ return GETPRIO_UPDTE(m, n, k); %}

BODY
{
    int tempmm = (m == (descA.mt-1)) ? (descA.m - m * descA.mb) : descA.mb;
    int tempnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int ldam   = BLKLDD( descA, m );

    printlog("thread %d VP %d CORE_zunmqr(%d, %d, %d)\n"
             "\t(PlasmaLeft, PlasmaConjTrans, tempmm, tempnn, min(tempmm, tempnn), ib, \n"
             "\t dataA(%d,%d)[%p], ldam, dataTS(%d,%d)[%p], descTS.mb, dataA(%d,%d)[%p], ldam, p_elem_A, descTS.nb)\n",
             context->th_id, context->virtual_process->vp_id, k, m, n, m, k, A, m, k, T, m, n, C);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_work );

    CORE_zunmqr(
        PlasmaLeft, PlasmaConjTrans,
        tempmm, tempnn, tempmm, ib,
        A /* dataA( m, k) */, ldam,
        T /* dataTS(m, k) */, descTS.mb,
        C /* dataA( m, n) */, ldam,
        p_elem_A, descTS.nb );

    dague_private_memory_push( p_work, p_elem_A );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END


/**
 * zttqrt()
 *
 * The row p kills the row m.
 * nextp is the row that will be killed by p at next stage of the reduction.
 * prevp is the row that has been killed by p at the previous stage of the reduction.
 * prevm is the row that has been killed by m at the previous stage of the reduction.
 * type defines the operation to perform: TS if 0, TT otherwise
 * ip is the index of the killer p in the sorted set of killers for the step k.
 * im is the index of the killer m in the sorted set of killers for the step k.
 *
 */
zttqrt(k, m)
  /* Execution space */
  k = 0   .. minMN-1
  m = k+1 .. descA.mt-1
  p =     inline_c %{ return qrtree.currpiv( &qrtree, k, m);    %}
  nextp = inline_c %{ return qrtree.nextpiv( &qrtree, k, p, m); %}
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, p, m); %}
  prevm = inline_c %{ return qrtree.prevpiv( &qrtree, k, m, m); %}
  type  = inline_c %{ return qrtree.gettype( &qrtree, k, m );   %}
  ip    = inline_c %{ return qrtree.geti(    &qrtree, k, p );   %}
  im    = inline_c %{ return qrtree.geti(    &qrtree, k, m );   %}

  SIMCOST inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? 6 : 2; %}

  : dataA(m, k)

  RW   A1 <- (   prevp == descA.mt ) ? A  zgeqrt(k, ip ) : A1 zttqrt(k, prevp )     [type = UPPER_TILE]
          -> (   nextp != descA.mt ) ? A1 zttqrt(k, nextp )                         [type = UPPER_TILE]
          -> ( ( nextp == descA.mt ) & (p == k) ) ? A zttqrt_out_A1(k)              [type = UPPER_TILE]
          -> ( ( nextp == descA.mt ) & (p != k) ) ? A2 zttqrt(k, p)                 [type = UPPER_TILE]

  RW   A2 <- ( (type == 0) && (k     == 0        ) ) ? dataA(m, k)                                      /* TS case */
          <- ( (type == 0) && (k     != 0        ) ) ? A2 zttmqr(k-1, m, k )                            /* TS case */
          <- ( (type != 0) && (prevm == descA.mt ) ) ? A  zgeqrt(k, im )            [type = UPPER_TILE] /* TT case */
          <- ( (type != 0) && (prevm != descA.mt ) ) ? A1 zttqrt(k, prevm )         [type = UPPER_TILE] /* TT case */

          -> (type == 0 ) ? dataA(m, k)
          -> (type != 0 ) ? dataA(m, k)                                             [type = UPPER_TILE]

          -> (type == 0) &&  (descA.nt-1 > k) ? V zttmqr(k, m, (k+1)..(descA.nt-1))
          -> (type != 0) &&  (descA.nt-1 > k) ? V zttmqr(k, m, (k+1)..(descA.nt-1)) [type = UPPER_TILE]

  RW   T  <- dataTT(m, k)                                              [type = LITTLE_T]
          -> dataTT(m, k)                                              [type = LITTLE_T]
          -> (descA.nt-1 > k)? T zttmqr(k, m, (k+1)..(descA.nt-1)) [type = LITTLE_T]

 ; inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? GETPRIO_PANEL(p, k) : GETPRIO_PANEL(m, k); %}

BODY
{
    int tempmm = ((m)==((descA.mt)-1)) ? ((descA.m)-(m*(descA.mb))) : (descA.mb);
    int tempkn = ((k)==((descA.nt)-1)) ? ((descA.n)-(k*(descA.nb))) : (descA.nb);
    int ldap = BLKLDD( descA, p );
    int ldam = BLKLDD( descA, m );

    printlog("thread %d VP %d CORE_zttqrt(%d, %d)\n"
             "\t(tempmm, tempkn, ib, dataA(%d,%d)[%p], A.mb, dataA(%d,%d)[%p], ldam, dataTT(%d,%d)[%p], descTT.mb, p_elem_A, p_elem_B)\n",
             context->th_id, context->virtual_process->vp_id, k, m, p, k, A1, m, k, A2, m, k, T);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_tau  );
    void *p_elem_B = dague_private_memory_pop( p_work );

    if ( type == DPLASMA_QR_KILLED_BY_TS ) {
        CORE_ztsqrt(
            tempmm, tempkn, ib,
            A1 /* dataA( p, k) */, ldap,
            A2 /* dataA( m, k) */, ldam,
            T  /* dataTT(m, k) */, descTT.mb,
            p_elem_A, p_elem_B );
    } else {
        CORE_zttqrt(
            tempmm, tempkn, ib,
            A1 /* dataA( p, k) */, ldap,
            A2 /* dataA( m, k) */, ldam,
            T  /* dataTT(m, k) */, descTT.mb,
            p_elem_A, p_elem_B );
    }
    dague_private_memory_push( p_tau , p_elem_A );
    dague_private_memory_push( p_work, p_elem_B );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END


zttqrt_out_A1(k) [profile = off]
  k = 0..( (descA.mt <= descA.nt) ? descA.mt-2 : descA.nt-1 )
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, k, k ); %}

  : dataA(k, k)

  RW A <- A1 zttqrt( k, prevp ) [type = UPPER_TILE]
       -> dataA(k, k)           [type = UPPER_TILE]
BODY
{
    /* nothing */
}
END

/**
 * zttmqr()
 *
 * See also zttqrt()
 * type1 defines the operations to perfom at next step k+1 on the row m
 *   if type1 == 0, it will be a TS so the tile goes to a TTQRT/TTMQR operation
 *   if type1 != 0, it will be a TT so the tile goes to a GEQRT/UNMQR operation
 * im1 is the index of the killer m at the next step k+1 if its type is !0, descA.mt otherwise
 *
 */
zttmqr(k, m, n)
  /* Execution space */
  k = 0   .. minMN-1
  m = k+1 .. descA.mt-1
  n = k+1 .. descA.nt-1
  p =     inline_c %{ return qrtree.currpiv( &qrtree, k,   m);    %}
  nextp = inline_c %{ return qrtree.nextpiv( &qrtree, k,   p, m); %}
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k,   p, m); %}
  prevm = inline_c %{ return qrtree.prevpiv( &qrtree, k,   m, m); %}
  type  = inline_c %{ return qrtree.gettype( &qrtree, k,   m );   %}
  type1 = inline_c %{ return qrtree.gettype( &qrtree, k+1, m );   %}
  ip    = inline_c %{ return qrtree.geti(    &qrtree, k,   p );   %}
  im    = inline_c %{ return qrtree.geti(    &qrtree, k,   m );   %}
  im1   = inline_c %{ return qrtree.geti(    &qrtree, k+1, m );   %}

  SIMCOST inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? 12 : 6; %}

  : dataA(m, n)

  RW   A1 <- (   prevp == descA.mt ) ? C  zunmqr( k, ip, n ) : A1 zttmqr(k, prevp, n )
          -> (   nextp != descA.mt ) ? A1 zttmqr( k, nextp, n)
          -> ( ( nextp == descA.mt ) & ( p == k ) ) ? A zttmqr_out_A1(p, n)
          -> ( ( nextp == descA.mt ) & ( p != k ) ) ? A2 zttmqr( k, p, n )

  RW   A2 <- ( (type  == 0 ) && (k     == 0        ) ) ? dataA(m, n)
          <- ( (type  == 0 ) && (k     != 0        ) ) ? A2 zttmqr(k-1, m, n )
          <- ( (type  != 0 ) && (prevm == descA.mt ) ) ? C  zunmqr(k, im, n)
          <- ( (type  != 0 ) && (prevm != descA.mt ) ) ? A1 zttmqr(k, prevm, n )

          -> ( (type1 != 0 ) && (n==(k+1)) ) ? A  zgeqrt( k+1, im1 )
          -> ( (type1 != 0 ) && (n>  k+1)  ) ? C  zunmqr( k+1, im1, n )
          -> ( (type1 == 0 ) && (n==(k+1)) ) ? A2 zttqrt( k+1, m )
          -> ( (type1 == 0 ) && (n> (k+1)) ) ? A2 zttmqr( k+1, m, n )

  READ  V <- (type == 0) ? A2 zttqrt(k, m)
          <- (type != 0) ? A2 zttqrt(k, m) [type = UPPER_TILE]

  READ  T <- T  zttqrt(k, m)               [type = LITTLE_T]

    ; inline_c %{ return type == DPLASMA_QR_KILLED_BY_TS ? GETPRIO_UPDTE(p, n, k) : GETPRIO_UPDTE(m, n, k); %}

BODY
{
    int tempnn = ((n)==((descA.nt)-1)) ? ((descA.n)-(n*(descA.nb))) : (descA.nb);
    int tempmm = ((m)==((descA.mt)-1)) ? ((descA.m)-(m*(descA.mb))) : (descA.mb);
    int ldap = BLKLDD( descA, p );
    int ldam = BLKLDD( descA, m );
    int ldwork = ib;

    printlog("thread %d VP %d CORE_zttmqr(%d, %d, %d)\n"
             "\t(PlasmaLeft, PlasmaConjTrans, descA.mb, tempnn, tempmm, tempnn, descA.nb, ib, \n"
             "\t dataA(%d,%d)[%p], A.mb, dataA(%d,%d)[%p], ldam, dataA(%d,%d)[%p], ldam, dataTT(%d,%d)[%p], descTT.mb, p_elem_A, ldwork)\n",
             context->th_id, context->virtual_process->vp_id, k, m, n, p, n, A1, m, n, A2, m, k, V, m, k, T);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_work );

    if ( type == DPLASMA_QR_KILLED_BY_TS ) {
        CORE_ztsmqr(
            PlasmaLeft, PlasmaConjTrans,
            descA.mb, tempnn, tempmm, tempnn, descA.nb, ib,
            A1 /* dataA( p, n) */, ldap,
            A2 /* dataA( m, n) */, ldam,
            V  /* dataA( m, k) */, ldam,
            T  /* dataTT(m, k) */, descTT.mb,
            p_elem_A, ldwork );
    } else {
        CORE_zttmqr(
            PlasmaLeft, PlasmaConjTrans,
            descA.mb, tempnn, tempmm, tempnn, descA.nb, ib,
            A1 /* dataA( p, n) */, ldap,
            A2 /* dataA( m, n) */, ldam,
            V  /* dataA( m, k) */, ldam,
            T  /* dataTT(m, k) */, descTT.mb,
            p_elem_A, ldwork );
    }
    dague_private_memory_push( p_work, p_elem_A );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zttmqr_out_A1(k, n) [profile = off]
  k = 0   .. minMN-2
  n = k+1 .. descA.nt-1
  prevp = inline_c %{ return qrtree.prevpiv( &qrtree, k, k, k ); %}

  : dataA(k, n)

  RW A <- A1 zttmqr( k, prevp, n )
       -> dataA(k, n)
BODY
{
    /* nothing */
}
END
