extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation. All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 * $COPYRIGHT
 *
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

#if defined(HAVE_RECURSIVE)
#include "data_dist/matrix/subtile.h"
#include "dague/recursive.h"
#endif

#if defined(HAVE_CUDA)
#include "dague/devices/cuda/dev_cuda.h"
#include "dplasma/cores/cuda_ztsmqr.h"
#endif  /* defined(HAVE_CUDA) */

%}

dataA  [type = "dague_ddesc_t *"]
dataT  [type = "dague_ddesc_t *" aligned=dataA]
ib     [type = "int"]
p_work [type = "dague_memory_pool_t *" size = "(sizeof(dague_complex64_t)*ib*descT.nb)"]
p_tau  [type = "dague_memory_pool_t *" size = "(sizeof(dague_complex64_t)   *descT.nb)"]

descA   [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
descT   [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataT)"]
smallnb [type = "int" hidden = on default = "descA.nb" ]

zgeqrt_typechange(k)  [profile = off]
  /* Execution space */
  k = 0 .. inline_c %{ return dplasma_imin((descA.nt-1),(descA.mt-1)); %}

  : dataA(k,k)

RW A <- A zgeqrt(k)
     -> (k < (descA.nt-1)) ? A zunmqr(k, (k+1)..(descA.nt-1)) [type = LOWER_TILE]
     -> dataA(k, k)                                           [type = LOWER_TILE]

BODY
{
    /* Nothing */
}
END

/**************************************************
 *                    zgeqrt                      *
 **************************************************/
zgeqrt(k)
  /* Execution space */
  k = 0 .. inline_c %{ return dplasma_imin((descA.nt-1),(descA.mt-1)); %}

  : dataA(k,k)

  RW    A <- (0 == k) ? dataA(k, k) : A2 ztsmqr(k-1, k, k)
          -> (descA.mt==(k+1)) ? dataA(k,k)                      [type = UPPER_TILE]
          -> (descA.mt>=(k+2)) ? A1 ztsqrt(k, k+1)               [type = UPPER_TILE]
          -> A zgeqrt_typechange(k)

  RW    T <- dataT(k, k)                                         [type = LITTLE_T]
          -> dataT(k, k)                                         [type = LITTLE_T]
          -> (descA.nt-1 > k) ? T zunmqr(k, (k+1)..(descA.nt-1)) [type = LITTLE_T]

  /* Priority */
  ;(descA.nt-k)*(descA.nt-k)*(descA.nt-k)

BODY [type=recursive]
{
    int tempkm = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int tempkn = (k == (descA.nt-1)) ? (descA.n - k * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );

    printlog("CORE_zgeqrt(%d)\n"
             "\t(tempkm, tempkn, ib, A(%d,%d)[%p], ldak, T(%d,%d)[%p], descT.mb, p_elem_A, p_elem_B)\n",
             k, k, k, A, k, k, T);

    if (tempkm > smallnb) {
        subtile_desc_t *small_descA;
        subtile_desc_t *small_descT;
        dague_handle_t *dague_zgeqrt;

        dague_data_transfer_ownership_to_copy(gA->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
        dague_data_transfer_ownership_to_copy(gT->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

        small_descA = subtile_desc_create( &(descA), k, k,
                                           dplasma_imin(descA.mb, ldak), smallnb,
                                           0, 0, tempkm, tempkn );
        small_descT = subtile_desc_create( &(descT), k, k,
                                           ib, smallnb,
                                           0, 0, ib, tempkn );

        small_descA->mat = A;
        small_descT->mat = T;

        /* dague_object */
        dague_zgeqrt = dplasma_zgeqrfr_geqrt_New( (tiled_matrix_desc_t *)small_descA,
                                                  (tiled_matrix_desc_t *)small_descT,
                                                  p_work );

        /* recursive call */
        dague_recursivecall( context, this_task,
                             dague_zgeqrt, dplasma_zgeqrfr_geqrt_Destruct,
                             2, small_descA, small_descT );

        return DAGUE_HOOK_RETURN_AGAIN;
    }
    else
        return DAGUE_HOOK_RETURN_NEXT;
}
END

BODY
{
    int tempkm = ((k)==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    int tempkn = ((k)==(descA.nt-1)) ? (descA.n-(k*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );

    dague_data_transfer_ownership_to_copy(gA->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
    dague_data_transfer_ownership_to_copy(gT->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_zgeqrt(%d)\n"
             "\t(tempkm, tempkn, ib, A(%d,%d)[%p], ldak, T(%d,%d)[%p], descT.mb, p_elem_A, p_elem_B)\n",
             k, k, k, A, k, k, T);

#if !defined(DAGUE_DRY_RUN)

    void *p_elem_A = dague_private_memory_pop( p_tau );
    void *p_elem_B = dague_private_memory_pop( p_work );

    CORE_zgeqrt(tempkm, tempkn, ib,
                A /* dataA(k,k) */, ldak,
                T /* dataT(k,k) */, descT.mb,
                p_elem_A, p_elem_B );

    dague_private_memory_push( p_tau,  p_elem_A );
    dague_private_memory_push( p_work, p_elem_B );

#endif  /* !defined(DAGUE_DRY_RUN) */
}
END


/**************************************************
 *                    zunmqr                      *
 **************************************************/
zunmqr(k,n)
  /* Execution space */
  k = 0   .. inline_c %{ return dplasma_imin((descA.nt-2),(descA.mt-1)); %}
  n = k+1 .. descA.nt-1

  : dataA(k,n)

  READ  A <- A zgeqrt_typechange(k)                        [type = LOWER_TILE]
  READ  T <- T zgeqrt(k)                                   [type = LITTLE_T]
  RW    C <- (k == 0) ? dataA(k, n) : A2 ztsmqr(k-1, k, n)
          -> (k == (descA.mt-1)) ? dataA(k, n)
          -> (k <  (descA.mt-1)) ? A1 ztsmqr(k, k+1, n)

BODY [type=recursive]
{
    int tempkm = ((k)==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    int tempnn = ((n)==(descA.nt-1)) ? (descA.n-(n*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );

    if (tempkm > smallnb) {
        subtile_desc_t *small_descA;
        subtile_desc_t *small_descT;
        subtile_desc_t *small_descC;
        dague_handle_t *dague_zunmqr_panel;

        dague_data_transfer_ownership_to_copy(gC->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

        small_descA = subtile_desc_create( &(descA), k, k,
                                           dplasma_imin(descA.mb, ldak), smallnb,
                                           0, 0, tempkm, tempkm );
        small_descC = subtile_desc_create( &(descA), k, n,
                                           dplasma_imin(descA.mb, ldak), smallnb,
                                           0, 0, tempkm, tempnn );
        small_descT = subtile_desc_create( &(descT), k, k,
                                           ib, smallnb,
                                           0, 0, ib, tempkm );

        small_descA->mat = A;
        small_descC->mat = C;
        small_descT->mat = T;

        /* dague_object */
        dague_zunmqr_panel = dplasma_zgeqrfr_unmqr_New( (tiled_matrix_desc_t *)small_descA,
                                                        (tiled_matrix_desc_t *)small_descT,
                                                        (tiled_matrix_desc_t *)small_descC,
                                                        p_work );

        /* recursive call */
        dague_recursivecall( context, this_task,
                             dague_zunmqr_panel, dplasma_zgeqrfr_unmqr_Destruct,
                             3, small_descA, small_descC, small_descT );

        return DAGUE_HOOK_RETURN_AGAIN;
    }
    else
        return DAGUE_HOOK_RETURN_NEXT;
}
END

BODY
{
    int tempkm = ((k)==(descA.mt-1)) ? (descA.m-(k*descA.mb)) : descA.mb;
    int tempnn = ((n)==(descA.nt-1)) ? (descA.n-(n*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );

    dague_data_transfer_ownership_to_copy(gC->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_zunmqr(%d, %d)\n"
             "\t(PlasmaLeft, PlasmaConjTrans, tempkm, tempnn, tempkm, ib, A(%d,%d)[%p], ldak, T(%d,%d)[%p], descT.mb, A(%d,%d)[%p], ldak, p_elem_A, descT.nb)\n",
             k, n, k, k, A, k, k, T, k, n, C);

#if !defined(DAGUE_DRY_RUN)

    void *p_elem_A = dague_private_memory_pop( p_work );

    CORE_zunmqr(PlasmaLeft, PlasmaConjTrans,
                tempkm, tempnn, tempkm, ib,
                A /* dataA(k,k) */, ldak,
                T /* dataT(k,k) */, descT.mb,
                C /* dataA(k,n) */, ldak,
                p_elem_A, descT.nb );

    dague_private_memory_push( p_work, p_elem_A );

#endif  /* !defined(DAGUE_DRY_RUN) */
}
END


ztsqrt_out_Ak(k) [profile = off]
  k = 0..( (descA.mt <= descA.nt) ? descA.mt-2 : descA.nt-1 )

  : dataA(k, k)

  RW A1 <- A1 ztsqrt(k, descA.mt-1) [type = UPPER_TILE]
        -> dataA(k, k)              [type = UPPER_TILE]
BODY
{
    /* nothing */
}
END

/**************************************************
 *                    ztsqrt                      *
 **************************************************/
ztsqrt(k,m)
  /* Execution space */
  k = 0   .. inline_c %{ return dplasma_imin((descA.nt-1),(descA.mt-2)); %}
  m = k+1 .. descA.mt-1

  : dataA(m, k)

  RW   A1 <- ( m == (k+1)       ) ? A zgeqrt(m-1) : A1 ztsqrt(k, m-1)       [type = UPPER_TILE]
          -> ( m == (descA.mt-1)) ? A1 ztsqrt_out_Ak(k) : A1 ztsqrt(k, m+1) [type = UPPER_TILE]

  RW   A2 <- (k == 0) ? dataA(m, k) : A2 ztsmqr(k-1, m, k)
          -> dataA(m, k)
          -> (k < (descA.nt-1)) ? V ztsmqr(k, m, (k+1)..(descA.nt-1))

  RW   T  <- dataT(m, k)                                                    [type = LITTLE_T]
          -> dataT(m, k)                                                    [type = LITTLE_T]
          -> (k < (descA.nt-1)) ? T ztsmqr(k, m, (k+1)..(descA.nt-1))       [type = LITTLE_T]

    /* Priority */
; (descA.mt-k)*(descA.mt-k)*(descA.mt-k)

BODY [type=recursive]
{
    int tempmm = ((m)==(descA.mt-1)) ? (descA.m-(m*descA.mb)) : descA.mb;
    int tempkn = ((k)==(descA.nt-1)) ? (descA.n-(k*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );

    if (tempmm > smallnb) {
        subtile_desc_t *small_descA1;
        subtile_desc_t *small_descA2;
        subtile_desc_t *small_descT;
        dague_handle_t *dague_ztsqrt;

        dague_data_transfer_ownership_to_copy(gA1->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
        dague_data_transfer_ownership_to_copy(gA2->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
        dague_data_transfer_ownership_to_copy(gT->original,  0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

        small_descA1 = subtile_desc_create( &(descA), k, k,
                                            dplasma_imin(descA.mb, ldak), smallnb,
                                            0, 0, tempkn, tempkn );
        small_descA2 = subtile_desc_create( &(descA), m, k,
                                            dplasma_imin(descA.mb, ldam), smallnb,
                                            0, 0, tempmm, tempkn );
        small_descT  = subtile_desc_create( &(descT), m, k,
                                            ib, smallnb,
                                            0, 0, ib, tempkn );

        small_descA1->mat = A1;
        small_descA2->mat = A2;
        small_descT->mat  = T;

        /* dague_object */
        dague_ztsqrt = dplasma_zgeqrfr_tsqrt_New((tiled_matrix_desc_t *)small_descA1,
                                                 (tiled_matrix_desc_t *)small_descA2,
                                                 (tiled_matrix_desc_t *)small_descT,
                                                 p_work, p_tau );

        /* recursive call */
        dague_recursivecall( context, this_task,
                             dague_ztsqrt, dplasma_zgeqrfr_tsqrt_Destruct,
                             3, small_descA1, small_descA2, small_descT );

        return DAGUE_HOOK_RETURN_AGAIN;
    }
    else
        return DAGUE_HOOK_RETURN_NEXT;
}
END

BODY
{
    int tempmm = ((m)==(descA.mt-1)) ? (descA.m-(m*descA.mb)) : descA.mb;
    int tempkn = ((k)==(descA.nt-1)) ? (descA.n-(k*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );

    dague_data_transfer_ownership_to_copy(gA1->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
    dague_data_transfer_ownership_to_copy(gA2->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
    dague_data_transfer_ownership_to_copy(gT->original,  0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_ztsqrt(%d, %d)\n"
             "\t(tempmm, tempkn, ib, A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldam, T(%d,%d)[%p], descT.mb, p_elem_A, p_elem_B)\n",
             k, m, k, k, T, m, k, A1, m, k, A2);

#if !defined(DAGUE_DRY_RUN)

    void *p_elem_A = dague_private_memory_pop( p_tau );
    void *p_elem_B = dague_private_memory_pop( p_work );

    CORE_ztsqrt(tempmm, tempkn, ib,
                A1 /* dataA(k,k) */, ldak,
                A2 /* dataA(m,k) */, ldam,
                T  /* dataT(m,k) */, descT.mb,
                p_elem_A, p_elem_B );

    dague_private_memory_push( p_tau,  p_elem_A );
    dague_private_memory_push( p_work, p_elem_B );

#endif  /* !defined(DAGUE_DRY_RUN) */
}
END


ztsmqr_out_A1(k, n) [profile = off]
  k = 0   .. inline_c %{ return dplasma_imin((descA.nt-2),(descA.mt-2)); %}
  n = k+1 .. descA.nt-1

  : dataA(k, n)

  RW A1 <- A1 ztsmqr(k, descA.mt-1, n)
       -> dataA(k, n)
BODY
{
    /* nothing */
}
END


/**************************************************
 *                    ztsmqr                      *
 **************************************************/
ztsmqr(k,m,n)
  /* Execution space */
  k = 0     .. inline_c %{ return dplasma_imin((descA.mt-2),(descA.nt-2)); %}
  m = (k+1) .. (descA.mt-1)
  n = (k+1) .. (descA.nt-1)

  : dataA(m, n)

  RW   A1 <- ( (k+1) == m ) ? C zunmqr(m-1, n) : A1 ztsmqr(k, m-1, n)
          -> ( m == (descA.mt-1) ) ? A1 ztsmqr_out_A1(k, n) : A1 ztsmqr(k, m+1, n)

  RW   A2 <- (0==k) ? dataA(m, n) : A2 ztsmqr(k-1, m, n)
          -> (((k+1) == n) & ((k+1) == m)) ? A  zgeqrt(n)            /* Diagonal */
          -> (((k+1) == m) & (    n > m )) ? C  zunmqr(k+1, n)       /* Next row */
          -> (((k+1) == n) & (    m > n )) ? A2 ztsqrt(n, m)         /* Next column */
          -> (((k+1) <  n) & ((1+k) < m )) ? A2 ztsmqr(k+1, m, n)    /* trailing submatrix */

  READ V  <- A2 ztsqrt(k, m)
  READ T  <- T  ztsqrt(k, m)   [type = LITTLE_T]

    /* Priority */
; (descA.mt-k)*(descA.mt-n)*(descA.mt-n)

BODY [type=CUDA dyld=dplasma_cuda_ztsmqr]
{
    int tempmm = ((m)==(descA.mt-1)) ? (descA.m-(m*descA.mb)) : descA.mb;
    int tempnn = ((n)==(descA.nt-1)) ? (descA.n-(n*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );
    int ret, pushout_A1 = 0, pushout_A2 = 0;

    if (m == descA.mt - 1) {
        pushout_A1 = 1;
    }
    if ((k+1) == m || (k+1) == n) {
        pushout_A2 = 1;
    }

#if defined(HAVE_MPI)
    pushout_A1 = 1;
#endif

    ret = gpu_ztsmqr( context, (dague_execution_context_t*)this_task,
                      pushout_A1, pushout_A2, m, n, k,
                      PlasmaLeft, PlasmaConjTrans,
                      descA.mb, tempnn, tempmm, tempnn, descA.nb, ib,
                      ldak, ldam, ldam, descT.mb );
    return ret;
}
END

BODY [type=recursive]
{
    int tempmm = (m == (descA.mt-1)) ? (descA.m - m * descA.mb) : descA.mb;
    int tempnn = (n == (descA.nt-1)) ? (descA.n - n * descA.nb) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );

    if (tempmm > smallnb) {
        subtile_desc_t *small_descA1;
        subtile_desc_t *small_descA2;
        subtile_desc_t *small_descV;
        subtile_desc_t *small_descT;
        dague_handle_t *dague_ztsmqr;

        dague_data_transfer_ownership_to_copy(gA1->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
        dague_data_transfer_ownership_to_copy(gA2->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

        small_descA1 = subtile_desc_create( &(descA), k, n,
                                            dplasma_imin(descA.mb, ldak), smallnb,
                                            0, 0, descA.mb, tempnn );
        small_descA2 = subtile_desc_create( &(descA), m, n,
                                            dplasma_imin(descA.mb, ldam), smallnb,
                                            0, 0, tempmm, tempnn );
        small_descV  = subtile_desc_create( &(descA), m, k,
                                            dplasma_imin(descA.mb, ldam), smallnb,
                                            0, 0, tempmm, descA.nb );
        small_descT  = subtile_desc_create( &(descT), m, k,
                                            ib, smallnb,
                                            0, 0, ib, descA.nb );

        small_descA1->mat = A1;
        small_descA2->mat = A2;
        small_descV->mat  = V;
        small_descT->mat  = T;

        /* dague_object */
        dague_ztsmqr = dplasma_zgeqrfr_tsmqr_New( (tiled_matrix_desc_t *)small_descA1,
                                                  (tiled_matrix_desc_t *)small_descA2,
                                                  (tiled_matrix_desc_t *)small_descV,
                                                  (tiled_matrix_desc_t *)small_descT,
                                                  p_work );

        /* recursive call */
        dague_recursivecall( context, this_task,
                             dague_ztsmqr, dplasma_zgeqrfr_tsmqr_Destruct,
                             4, small_descA1, small_descA2, small_descV, small_descT );

        return DAGUE_HOOK_RETURN_AGAIN;
    }
    else
        return DAGUE_HOOK_RETURN_NEXT;
}
END

BODY
{
    int tempmm = ((m)==(descA.mt-1)) ? (descA.m-(m*descA.mb)) : descA.mb;
    int tempnn = ((n)==(descA.nt-1)) ? (descA.n-(n*descA.nb)) : descA.nb;
    int ldak = BLKLDD( descA, k );
    int ldam = BLKLDD( descA, m );
    int ldwork = ib;

    dague_data_transfer_ownership_to_copy(gA1->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);
    dague_data_transfer_ownership_to_copy(gA2->original, 0 /* device */, FLOW_ACCESS_READ | FLOW_ACCESS_WRITE);

    printlog("CORE_ztsmqr(%d, %d, %d)\n"
             "\t(PlasmaLeft, PlasmaConjTrans, descA.mb, tempnn, tempmm, tempnn, descA.nb, ib, A(%d,%d)[%p], ldak, A(%d,%d)[%p], ldam, A(%d,%d)[%p], ldam, T(%d,%d)[%p], descT.mb, p_elem_A, ldwork)\n",
             k, m, n, k, n, A1, m, n, A2, m, k, V, m, k, T);

#if !defined(DAGUE_DRY_RUN)
    void *p_elem_A = dague_private_memory_pop( p_work );

    CORE_ztsmqr(PlasmaLeft, PlasmaConjTrans,
                descA.mb, tempnn, tempmm, tempnn, descA.nb, ib,
                A1 /* dataA(k,n) */, ldak,
                A2 /* dataA(m,n) */, ldam,
                V  /* dataA(m,k) */, ldam,
                T  /* dataT(m,k) */, descT.mb,
                p_elem_A, ldwork );

    dague_private_memory_push( p_work, p_elem_A );

#endif  /* !defined(DAGUE_DRY_RUN) */
}
END
