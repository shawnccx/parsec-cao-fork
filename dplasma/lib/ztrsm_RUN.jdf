extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> s d c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

side      [type = "PLASMA_enum"]
uplo      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
diag      [type = "PLASMA_enum"]
alpha     [type = "dague_complex64_t"]
dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
dataB     [type = "dague_ddesc_t *"]
descB     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataB)"]


ztrsm(k,m)
  /* Execution space */
  k = 0..(descB.nt-1)
  m = 0..(descB.mt-1)

  : dataB(m,k)

  READ  A <- A ztrsm_in_A0(k)

  RW    B <- (0==k) ? dataB(m,k)
          <- (k>=1) ? E zgemm(k-1, m, k)
          -> (descB.nt>=(k+2)) ? C zgemm(k, m, (k+1) .. (descB.nt-1))
          -> dataB(m,k)

BODY
{
    int tempmm = ((m)==(descB.mt-1)) ? (descB.m-(m*descB.mb)) : descB.mb;
    int tempkn = ((k)==(descB.nt-1)) ? (descB.n-(k*descB.nb)) : descB.nb;
    dague_complex64_t lalpha = ((k)==(0)) ? (alpha) : (dague_complex64_t)1.0;
    int lda = BLKLDD( descA, k );
    int ldb = BLKLDD( descB, m );

#if !defined(DAGUE_DRY_RUN)
        CORE_ztrsm(side, uplo, trans, diag,
                   tempmm, tempkn, lalpha,
                   A /* dataA(k,k) */, lda,
                   B /* dataB(m,k) */, ldb );
#endif /* !defined(DAGUE_DRY_RUN) */

    printlog("CORE_ztrsm(%d, %d)\n"
             "\t(side, uplo, trans, diag, tempmm, tempkn, lalpha, dataA(%d,%d)[%p], lda, dataB(%d,%d)[%p], ldb)\n",
             k, m, k, k, A, m, k, B);
}
END

/*
 * Pseudo-task
 */
ztrsm_in_A0(k) [profile = off]
  k = 0 .. (descB.nt-1)

  : dataA(k,k)

  RW A <- dataA(k,k)
       -> A ztrsm(k,0 .. (descB.mt-1))
BODY
{
    /* nothing */
}
END


zgemm(k,m,n)
  /* Execution space */
  k = 0 .. (descB.nt-2)
  m = 0 .. (descB.mt-1)
  n = (k+1) .. (descB.nt-1)

  : dataB(m,n)

  READ  C <- B ztrsm(k, m)
  READ  D <- D zgemm_in_A0(k,n)

  RW    E <- (0==k) ? dataB(m,n)
          <- (k>=1) ? E zgemm(k-1, m, n)
          -> ((1+k)==n) ? B ztrsm(n, m)
          -> (n>=(2+k)) ? E zgemm(k+1, m, n)

BODY
{
    int tempmm = ((m)==(descB.mt-1)) ? (descB.m-(m*descB.mb)) : descB.mb;
    int tempnn = ((n)==(descB.nt-1)) ? (descB.n-(n*descB.nb)) : descB.nb;
    int ldb = BLKLDD( descB, m );
    int lda = BLKLDD( descA, k );

    dague_complex64_t lalpha = ((k)==(0)) ? (alpha) : (dague_complex64_t)1.0;

#if !defined(DAGUE_DRY_RUN)
        CORE_zgemm(PlasmaNoTrans, PlasmaNoTrans,
                   tempmm, tempnn, descB.mb,
                   -1.0,   C /* dataB(m,k) */, ldb,
                           D /* dataA(k,n) */, lda,
                   lalpha, E /* dataB(m,n) */, ldb );
#endif /* !defined(DAGUE_DRY_RUN) */

    printlog("CORE_zgemm(%d, %d, %d)\n"
             "\t(PlasmaNoTrans, PlasmaNoTrans, tempmm, tempnn, descB.mb, mzone, dataB(%d,%d)[%p], ldb, dataA(%d,%d)[%p], lda, lalpha, dataB(%d,%d)[%p], ldb)\n",
             k, m, n, m, k, C, k, n, D, m, n, E);
}
END

/*
 * Pseudo-task
 */
zgemm_in_A0(k,n) [profile = off]
  k = 0 .. (descB.nt-2)
  n = (k+1) .. (descB.nt-1)

: dataA(k, n)

    RW D <- dataA(k, n)
       -> D zgemm(k,0 .. (descB.mt-1),n)
BODY
{
    /* nothing */
}
END
