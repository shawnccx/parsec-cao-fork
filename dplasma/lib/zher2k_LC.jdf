extern "C" %{
/*
 *  Copyright (c) 2010-2013
 *
 *  The University of Tennessee and The University
 *  of Tennessee Research Foundation.  All rights
 *  reserved.
 *
 * @precisions normal z -> z c
 *
 */
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

uplo      [type = "PLASMA_enum"]
trans     [type = "PLASMA_enum"]
alpha     [type = "dague_complex64_t"]
dataA     [type = "dague_ddesc_t *"]
descA     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataA)"]
dataB     [type = "dague_ddesc_t *"]
descB     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataB)"]
beta      [type = "double"]
dataC     [type = "dague_ddesc_t *"]
descC     [type = "tiled_matrix_desc_t" hidden = on default = "*((tiled_matrix_desc_t*)dataC)"]


zher2k(n, k)
  /* Execution Space */
  n = 0 .. descC.nt-1
  k = 0 .. descA.mt-1

  /* Locality */
  : dataC(n,n)

  READ  A    <- A in_data_A(k, n)
  READ  B    <- B in_data_B(k, n)
  RW    C    <- (k == 0) ? dataC(n,n)
             <- (k >  0) ? C zher2k(n, k-1)
             -> (k <  (descA.mt-1)) ? C zher2k(n, k+1)
             -> (k == (descA.mt-1)) ? dataC(n,n)

BODY
{
    int tempnn = (n == (descC.nt-1)) ? (descC.n - n * descC.nb) : descC.nb;
    int tempkn = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int ldbk = BLKLDD( descB, k );
    int ldcn = BLKLDD( descC, n );
    double dbeta = (k == 0) ? beta : (double)1.;

    printlog("CORE_zher2k(%d, %d)\n"
             "\t(uplo, trans, tempnn, tempkn, alpha, A(%d,%d)[%p], ldan, B(%d,%d)[%p], ldbn, dbeta, C(%d,%d)[%p], ldcn)\n",
             n, k, k, n, A, k, n, B, n, n, C);
#if !defined(DAGUE_DRY_RUN)
    CORE_zher2k(uplo, trans, tempnn, tempkn,
                alpha, A /* dataA(k,n) */, ldak,
                       B /* dataB(k,n) */, ldbk,
                dbeta, C /* dataC(n,n) */, ldcn );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zgemm_AB(m, n, k)
  /* Execution Space */
  m = 1   .. descC.mt-1
  n = 0   .. m-1
  k = 0   .. descA.mt-1

  /* Locality */
  : dataC(m,n)

  READ  A    <- A in_data_A(k, m)
  READ  B    <- B in_data_B(k, n)
  RW    C    <- (k == 0) ? dataC(m, n)
             <- (k >  0) ? C zgemm_BA(m, n, k-1)
             -> C zgemm_BA(m, n, k)

BODY
{
    int tempmm = (m == (descC.mt-1)) ? (descC.m - m * descC.mb) : descC.mb;
    int tempnn = (n == (descC.nt-1)) ? (descC.n - n * descC.nb) : descC.nb;
    int tempkn = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int ldbk = BLKLDD( descB, k );
    int ldcm = BLKLDD( descC, m );
    dague_complex64_t zbeta = (k == 0) ? ((dague_complex64_t)beta) : (dague_complex64_t)1.;

    printlog("CORE_zgemm_AB(%d, %d, %d)\n"
             "\t(PlasmaconjTrans, PlasmaNoTrans, m=%d, n=%d, k=%d, alpha=(%e,%e), A(%d,%d)[%p], lda=%d, B(%d,%d)[%p], ldb=%d, beta=(%e,%e), C(%d,%d)[%p], ldc=%d)\n",
             m, n, k, tempmm, tempnn, tempkn,
             creal(alpha), cimag(alpha), k, m, A, ldak, k, n, B, ldbk,
             creal(zbeta), cimag(zbeta), m, n, C, ldcm);
#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(PlasmaConjTrans, PlasmaNoTrans,
               tempmm, tempnn, tempkn,
               alpha, A /* dataA(k,m) */, ldak,
                      B /* dataB(k,n) */, ldbk,
               zbeta, C /* dataC(m,n) */, ldcm );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

zgemm_BA(m, n, k)
  /* Execution Space */
  m = 1   .. descC.mt-1
  n = 0   .. m-1
  k = 0   .. (descA.mt-1)

  /* Locality */
  : dataC(m,n)

  READ  A    <- B in_data_B(k, m)
  READ  B    <- A in_data_A(k, n)
  RW    C    <- C zgemm_AB(m, n, k)
             -> (k <  (descA.mt-1)) ? C zgemm_AB(m, n, k+1)
             -> (k == (descA.mt-1)) ? dataC(m, n)

BODY
{
    int tempmm = (m == (descC.mt-1)) ? (descC.m - m * descC.mb) : descC.mb;
    int tempnn = (n == (descC.nt-1)) ? (descC.n - n * descC.nb) : descC.nb;
    int tempkn = (k == (descA.mt-1)) ? (descA.m - k * descA.mb) : descA.mb;
    int ldak = BLKLDD( descA, k );
    int ldbk = BLKLDD( descB, k );
    int ldcm = BLKLDD( descC, m );

    printlog("CORE_zgemm_BA(%d, %d, %d)\n"
             "\t(PlasmaConjTrans, PlasmaNoTrans, m=%d, n=%d, k=%d, alpha=(%e,%e), B(%d,%d)[%p], ldb=%d, A(%d,%d)[%p], lda=%d, beta=(%e,%e), C(%d,%d)[%p], ldc=%d)\n",
             m, n, k, tempmm, tempnn, tempkn,
             creal(alpha), cimag(alpha), k, m, A, ldbk, k, n, B, ldak,
             creal(1.),    cimag(1.),    m, n, C, ldcm);

#if !defined(DAGUE_DRY_RUN)
    CORE_zgemm(PlasmaConjTrans, PlasmaNoTrans,
               tempmm, tempnn, tempkn,
               conj(alpha), A /* dataB(m,k) */, ldbk,
                            B /* dataA(n,k) */, ldak,
               1.,          C /* dataC(m,n) */, ldcm );
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

in_data_A(m, n) [profile = off]
  /* Execution Space */
  m = 0 .. (descA.mt-1)
  n = 0 .. (descA.nt-1)

  /* Locality */
  : dataA(m,n)

  READ  A    <- dataA(m,n)
             -> A zher2k(n, m)
             -> A zgemm_AB(n, 0 .. n-1, m)
             -> B zgemm_BA(n+1 .. descC.mt-1, n, m)

BODY
{
    /* nothing */
}
END

in_data_B(m, n) [profile = off]
  /* Execution Space */
  m = 0 .. (descB.mt-1)
  n = 0 .. (descB.nt-1)

  /* Locality */
  : dataB(m,n)

  READ  B    <- dataB(m,n)
             -> B zher2k(n, m)
             -> B zgemm_AB(n+1 .. descC.mt-1, n, m)
             -> A zgemm_BA(n, 0 .. n-1, m)

BODY
{
    /* nothing */
}
END
