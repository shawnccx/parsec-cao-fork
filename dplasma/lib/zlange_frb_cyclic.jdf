extern "C" %{
/*
 * Copyright (c) 2010-2013 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 * Copyright (c) 2013      Inria. All rights reserved.
 *
 * @precisions normal z -> s d c
 *
 *
 * This jdf returns the value of the Max, Infinite or Frobenius norm of a matrix A
 * where:
 *    - the max norm of a matrix is the maximum absolute value of all elements.
 *    - the infinite norm is the maximum of the sum of absolute values of elements in a same row
 *    - the Frobenius norm of a matrix is the square root of sum of squares.
 *
 * This jdf is optimized for 2D-Block cyclic distributed data with a grid
 * P-by-Q.
 * The first step searches the norm of each local rows of tile.
 * The second reduces the tile rows norms. At the end of this step, all Q
 * processes belonging to a row have the same data.
 * The third step combines the local tiles per column (one column per node).
 * The fourth step combines the norms together. At the end all processes
 * owns the same value.
 *
 * The reductions are done by a pipeline followed by a broadcast of the results.
 *
 * More information in zlange.f and zlassq.f from Netlib LAPACK.
 *
 * Details on W:
 *    - PlasmaMaxNorm: W[0) stores the maximum absolute value encountered
 *    - PlasmaInfNorm: In first steps, sums of set of rows, then identical to Max.
 *    - PlasmaFrbNorm: W[0] corresponds to scale and W[1] to sum.
 *
 */
#include <math.h>
#include "dplasma/lib/dplasmajdf.h"
#include "data_dist/matrix/matrix.h"

%}

/* Globals
 */
P            [type = "int"]
Q            [type = "int"]
ntype        [type = "PLASMA_enum"]
uplo         [type = "PLASMA_enum"]
diag         [type = "PLASMA_enum"]
dataA        [type = "dague_ddesc_t *"]
Tdist        [type = "dague_ddesc_t *"]
norm         [type = "double *"]

descA        [type = "tiled_matrix_desc_t" hidden=on default="*((tiled_matrix_desc_t*)dataA)" ]
minMNT       [type = "int" hidden=on default="dplasma_imin( descA.mt, descA.nt )" ]
minMN        [type = "int" hidden=on default="dplasma_imin( descA.m,  descA.n )"  ]
MT           [type = "int" hidden=on default="(uplo == PlasmaUpper) ? minMNT : descA.mt"]
NT           [type = "int" hidden=on default="(uplo == PlasmaLower) ? minMNT : descA.nt"]
M            [type = "int" hidden=on default="(uplo == PlasmaUpper) ? minMN  : descA.m"]
N            [type = "int" hidden=on default="(uplo == PlasmaLower) ? minMN  : descA.n"]

RWCOL(m, n) [profile = off]

    // Execution space
    m = 0 .. MT-1
    n = 0 .. Q-1
    col = inline_c %{
    switch (uplo) {
    case PlasmaLower:
    {
        if ( n > m )
            return NT;
        else {
            int mmax = dplasma_imin( m, NT-1 );
            return mmax-((mmax-n)%Q);
        }
    }
    break;
    case PlasmaUpper:
    {
        int tmpn = NT-((NT-n-1)%Q)-1;
        if (( n > NT-1 ) || ( tmpn < m ))
            return NT;
        else
            return tmpn;
    }
    break;
    default:
      if ( n > NT-1 )
          return NT;
      else
          return NT-((NT-n-1)%Q)-1;
    }
    %}

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    WRITE W -> (col < NT  ) ? W STEP1(m, col)  [type = COL]
            -> (col > NT-1) ? W STEP2(m, n)    [type = COL]

BODY
{
    printlog( "zlange RWCOL(%d, %d) [%d]\n", m, n, col);

#if !defined(DAGUE_DRY_RUN)
    /* Initialize W */
    switch( ntype ) {
    case PlasmaMaxNorm:
        ((double *)W)[0] = 0.;
        break;

    case PlasmaInfNorm:
        memset( W, 0, descA.mb * sizeof(double) );
        break;

    case PlasmaFrobeniusNorm:
        /* Initialize W */
        ((double *)W)[0] = 0.;
        ((double *)W)[1] = 1.;
        break;

    default:
        fprintf(stderr, "Unknown norm %d\n", ntype );
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

RWELT(m, n) [profile = off]

    // Execution space
    m = 0 .. P-1
    n = 0 .. Q-1
    row = inline_c %{
      return MT-((MT-m-1)%P)-1;
    %}

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    WRITE W -> (m <  MT   ) ? W STEP3(row, n)  [type = ELT]
            -> (m > (MT-1)) ? W STEP4(m,   n)  [type = ELT]

BODY
{
    printlog( "zlange RWELT(%d, %d) [%d]\n", m, n, row);

#if !defined(DAGUE_DRY_RUN)
    /* Initialize W */
    switch( ntype ) {
    case PlasmaMaxNorm:
    case PlasmaInfNorm:
        ((double *)W)[0] = 0.;
        break;

    case PlasmaFrobeniusNorm:
        /* Initialize W */
        ((double *)W)[0] = 0.;
        ((double *)W)[1] = 1.;
        break;
    default:
        fprintf(stderr, "Unknown norm %d\n", ntype );
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *
 *                    STEP 1
 *
 *  For j in [1,Q], W(m, j) = reduce( A(m, j+k*Q) )
 *
 **************************************************/
STEP1(m, n)

    // Execution space
    m = 0 .. MT-1
    nmin = inline_c %{ if (uplo == PlasmaUpper ) return m; else return 0; %}
    nmax = inline_c %{ if (uplo == PlasmaLower ) return dplasma_imin(m, NT-1); else return NT-1; %}
    n = nmin .. nmax

    // Parallel partitioning
    :dataA(m, n)

    // Parameters
    READ A <- dataA(m, n)
    RW   W <- ( n < (nmax+1-Q)) ? W STEP1( m, n+Q ) : W RWCOL( m, n%Q ) [type = COL]
           -> ( n < (nmin+Q)  ) ? W STEP2( m, n%Q ) : W STEP1( m, n-Q ) [type = COL]

BODY
{
    double *dW = (double*)W;
    int tempmm = ( m == (MT-1) ) ? M - m * descA.mb : descA.mb;
    int tempnn = ( n == (NT-1) ) ? N - n * descA.nb : descA.nb;
    int ldam = BLKLDD( descA, m );

    printlog("thread %d zlange STEP1(%d, %d)\n"
             "\t( tempmm=%d, tempnn=%d, A(%d, %d)[%p], lda=%d, W(%d,%d)[%p])\n",
             context->th_id, m, n, tempmm, tempnn, m, n, A, ldam, m, n%Q, W);

#if !defined(DAGUE_DRY_RUN)
    /*
     * Max norm
     */
    switch (ntype) {
    case PlasmaMaxNorm:
    {
        double lnorm = 0.;
        if ( (n == m)  && (uplo != PlasmaUpperLower) ) {
            CORE_zlantr(PlasmaMaxNorm, uplo, diag, tempmm, tempnn,
                        A, ldam, NULL, &lnorm);
        } else {
            CORE_zlange(PlasmaMaxNorm, tempmm, tempnn,
                        A, ldam, NULL, &lnorm);
        }
        *dW = ( lnorm > *dW ) ? lnorm : *dW;
    }
    break;

    /*
     * Infinite norms
     */
    case PlasmaInfNorm:
    {
        if ( (m == n) && (uplo != PlasmaUpperLower) ) {
            CORE_ztrasm( PlasmaRowwise, uplo, diag,
                         tempmm, tempnn,
                         A, ldam, W);
        }
        else {
            CORE_dzasum(PlasmaRowwise, PlasmaUpperLower,
                        tempmm, tempnn,
                        A, ldam, W);
        }
    }
    break;

    /*
     * Frobenius Norm
     */
    case PlasmaFrobeniusNorm:
    {
        if ( (n == m)  && (uplo != PlasmaUpperLower) ) {
            CORE_ztrssq( uplo, diag, tempmm, tempnn,
                         A, ldam, dW, dW+1);
        } else {
            CORE_zgessq( tempmm, tempnn,
                         A, ldam, dW, dW+1 );
        }
    }
    break;

    default:
        fprintf(stderr, "Unknown norm %d\n", ntype );
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *                    STEP 2
 *
 *  For each j, W(m, j) = reduce( W(m, 0..Q-1) )
 *
 **************************************************/
STEP2(m, n)

    // Execution space
    m = 0 .. MT-1
    n = 0 .. Q-1
    nmin = inline_c %{ if (uplo == PlasmaUpper ) return m; else return 0; %}
    col = inline_c %{
      return nmin + n - nmin%Q;
    %}

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ A <- ( n   ==  0     ) ? Tdist(m, n)        : W STEP2( m, n-1 ) [type = COL]
    RW   W <- ( col >  (NT-1) ) ? W RWCOL(m, n )     : W STEP1( m, col ) [type = COL]
           -> ( n   == (Q-1)  ) ? A STEP3(m, 0..Q-1) : A STEP2( m, n+1 ) [type = COL]

BODY
{
    double *dA = (double*)A;
    double *dW = (double*)W;

    printlog( "zlange STEP2(%d, %d) [%d, %d]\n"
              "\t( W(%d, %d) || W(%d, %d) )\n",
              m, n, nmin, col, m, n-1, m, n);

#if !defined(DAGUE_DRY_RUN)
    if(n > 0)
    {
        switch( ntype ) {
        case PlasmaMaxNorm:
        {
            *dW = ( *dA > *dW ) ? *dA : *dW;
        }
        break;

        case PlasmaInfNorm:
        {
            int tempmm = ( m == (MT-1) ) ? M - m * descA.mb : descA.mb;
            cblas_daxpy( tempmm, 1., A, 1, W, 1);
        }
        break;

        case PlasmaFrobeniusNorm:
        {
            if( dW[0] < dA[0] ) {
                dW[1] = dA[1] + (dW[1] * (( dW[0] / dA[0] ) * ( dW[0] / dA[0] )));
                dW[0] = dA[0];
            } else {
                dW[1] = dW[1] + (dA[1] * (( dA[0] / dW[0] ) * ( dA[0] / dW[0] )));
            }
        }
        break;

        default:
            fprintf(stderr, "Unknown norm %d\n", ntype );
        }
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *
 *                    STEP3
 *
 * For m in 0..P-1, W(m, n) = max( W(m..mt[P], n ) )
 *
 **************************************************/
STEP3(m, n)

    // Execution space
    m = 0 .. MT-1
    n = 0 .. Q-1

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ A <- W STEP2(m, Q-1)                                      [type = COL]
    RW   W <- (m < (MT-P)) ? W STEP3( m+P, n ) : W RWELT( m%P, n ) [type = ELT]
           -> (m < P     ) ? W STEP4( m,   n ) : W STEP3( m-P, n ) [type = ELT]

BODY
{
    double *dA = (double*)A;
    double *dW = (double*)W;

    printlog( "zlange STEP3(%d, %d)\n", m, n);

#if !defined(DAGUE_DRY_RUN)
    switch( ntype ) {
    case PlasmaMaxNorm:
    {
        if ( m < (MT-P) ) {
            *dW = ( *dA > *dW ) ? *dA : *dW;
        } else {
            *dW = *dA;
        }
    }
    break;

    case PlasmaInfNorm:
    {
        int tempmm = ( m == (MT-1) ) ? M - m * descA.mb : descA.mb;
        double maxval = 0;
        int i;

        for(i = 0; i < tempmm; i++, dA++)
            maxval = ( maxval > *dA ) ? maxval : *dA;

        if ( m < (MT-P) ) {
            *dW = ( maxval > *dW ) ? maxval : *dW;
        } else {
            *dW = maxval;
        }
    }
    break;

    case PlasmaFrobeniusNorm:
    {
        if( dW[0] < dA[0] ) {
            dW[1] = dA[1] + (dW[1] * (( dW[0] / dA[0] ) * ( dW[0] / dA[0] )));
            dW[0] = dA[0];
        } else {
            dW[1] = dW[1] + (dA[1] * (( dA[0] / dW[0] ) * ( dA[0] / dW[0] )));
        }
    }
    break;

    default:
        fprintf(stderr, "Unknown norm %d\n", ntype );
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *                    STEP 4
 *
 *  For each i, W(i, n) = max( W(0..P-1, n) )
 *
 **************************************************/
STEP4(m,n)

    // Execution space
    m = 0..P-1
    n = 0..Q-1

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ A <- ( m ==  0     ) ?   Tdist(m, n) /* Unused */ : W STEP4(m-1, n) [type = ELT]
    RW   W <- ( m >  (MT-1) ) ? W RWELT(m, n)              : W STEP3(m,   n) [type = ELT]
           -> ( m == (P-1)  ) ? W WRITE_RES(0..P-1, n)     : A STEP4(m+1, n) [type = ELT]

BODY
{
    printlog("thread %d zlange STEP4(%d, %d)\n",
             context->th_id, m, n);

#if !defined(DAGUE_DRY_RUN)
    double *dA = (double*)A;
    double *dW = (double*)W;
    if(m > 0) {
        switch( ntype ) {
        case PlasmaMaxNorm:
        case PlasmaInfNorm:
        {
            *dW = ( *dA > *dW ) ? *dA : *dW;
        }
        break;

        case PlasmaFrobeniusNorm:
        {
            double  sqr;

            if( dW[0] < dA[0] ) {
                sqr = dW[0] / dA[0];
                sqr = sqr * sqr;
                dW[1] = dA[1] + sqr * dW[1];
                dW[0] = dA[0];
            } else {
                sqr = dA[0] / dW[0];
                sqr = sqr * sqr;
                dW[1] = dW[1] + sqr * dA[1];
            }
        }
        break;

        default:
            fprintf(stderr, "Unknown norm %d\n", ntype );
        }
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END

/**************************************************
 *                      STEP5                     *
 **************************************************/
WRITE_RES(m,n)

    // Execution space
    m = 0..P-1
    n = 0..Q-1

    // Parallel partitioning
    :Tdist(m, n)

    // Parameters
    READ W <- W STEP4( P-1, n )   [type = ELT]

BODY
{
    printlog("thread %d zlange STORE RESULT(%d, %d)\n",
             context->th_id, m, n);

#if !defined(DAGUE_DRY_RUN)
    double *dW = (double*)W;

    switch( ntype ) {
    case PlasmaMaxNorm:
    case PlasmaInfNorm:
    {
        *norm = *dW;
    }
    break;

    case PlasmaFrobeniusNorm:
    {
        *norm = dW[0] * dplasma_dsqrt( dW[1] );
    }
    break;

    default:
        fprintf(stderr, "Unknown norm %d\n", ntype );
    }
#endif /* !defined(DAGUE_DRY_RUN) */
}
END
