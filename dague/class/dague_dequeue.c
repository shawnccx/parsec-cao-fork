/*
 * Copyright (c) 2013      The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 */

#include <dague_config.h>
#include "dague/class/dequeue.h"

OBJ_CLASS_INSTANCE(dague_dequeue_t, dague_list_t, 
                   NULL, NULL);

