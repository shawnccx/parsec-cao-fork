/*
 * Copyright (c) 2013      The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 */

#include <dague_config.h>
#include "dague/class/fifo.h"

OBJ_CLASS_INSTANCE(dague_fifo_t, dague_list_t,
                   NULL, NULL);
