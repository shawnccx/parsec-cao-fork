/*
 * Copyright (c) 2012-2015 The University of Tennessee and The University
 *                         of Tennessee Research Foundation.  All rights
 *                         reserved.
 */

#ifndef DAGUE_COLORS_H
#define DAGUE_COLORS_H

DAGUE_DECLSPEC char *unique_color(int index, int colorspace);

#endif  /* DAGUE_COLORS_H */


